package org.example;

import com.example.models.Money;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;

@Slf4j
public class StreamObserverMoneyImpl implements StreamObserver<Money> {
    private CountDownLatch countDownLatch;
    public StreamObserverMoneyImpl(CountDownLatch countDownLatch){
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void onNext(Money money) {
        log.info("Received Money - {}",money.getValue());
    }

    @Override
    public void onError(Throwable throwable) {
        log.info("Error happened - {}",throwable.getMessage());
        this.countDownLatch.countDown();
    }

    @Override
    public void onCompleted() {
        log.info("Money Transfer completed !!");
        this.countDownLatch.countDown();
    }
}
