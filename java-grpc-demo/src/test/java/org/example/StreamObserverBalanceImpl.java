package org.example;

import com.example.models.Balance;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;

@Slf4j
public class StreamObserverBalanceImpl implements StreamObserver<Balance> {
    private CountDownLatch countDownLatch;

    public StreamObserverBalanceImpl(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void onNext(Balance balance) {
    log.info("Getting the Balance - {}",balance.getAmount());
    }

    @Override
    public void onError(Throwable throwable) {
        countDownLatch.countDown();
    }

    @Override
    public void onCompleted() {
log.info("Server is Done !!");
        countDownLatch.countDown();
    }
}
