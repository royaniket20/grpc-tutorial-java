package org.example;

import com.example.models.Balance;
import com.example.models.BalanceCheckRequest;
import com.example.models.BankServiceGrpc;
import com.example.models.DepositRequest;
import com.example.models.ErrorResponse;
import com.example.models.Money;
import com.example.models.WithDrawRequest;
import com.google.common.util.concurrent.Uninterruptibles;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.protobuf.ProtoUtils;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
public class BankClientTest {

    private BankServiceGrpc.BankServiceBlockingStub bankServiceBlockingStub;
    private BankServiceGrpc.BankServiceStub bankServiceStubAsync;
//Setting Up client
    @BeforeAll
    public void setup(){
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("localhost", 5656)
                .usePlaintext()
                .build();
        log.info(" --- CHANNEL IS CREATED ----------"); //But they get just created - It will Not fail even if no server running on 5656
       this.bankServiceBlockingStub = BankServiceGrpc.newBlockingStub(managedChannel);
       this.bankServiceStubAsync = BankServiceGrpc.newStub(managedChannel);
        log.info(" --- BANKING SYNC AND ASYNC STUB IS CREATED ----------");
    }

    @Test
    public void BalanceTest(){
        try{
        Uninterruptibles.sleepUninterruptibly(5000,TimeUnit.MILLISECONDS);
        BalanceCheckRequest balanceCheckRequest = BalanceCheckRequest.newBuilder()
                .setAccountNumber(406)
                .build();
        Balance balance = this.bankServiceBlockingStub.getBalance(balanceCheckRequest);
        log.info("Received Balance - {}", balance.getAmount());

        }catch (Exception ex){
            Status status = Status.fromThrowable(ex);
            log.info(status.getCode() + " : " + status.getDescription());
            Metadata metadata = Status.trailersFromThrowable(ex);
            ErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ErrorResponse.getDefaultInstance()));
            log.info(errorResponse.getStatus() + " : " + errorResponse.getMessage());
        }

        //TODO - mORE ERROR HANDLING example here -https://www.vinsguru.com/grpc-error-handling/

    }

    @Test
    public void WithdrawTestBlocking(){
        WithDrawRequest withDrawRequest = WithDrawRequest.newBuilder()
                .setAccountNumber(58)
                .setAmount(10)
                .setDenomination(25)
                .build();
        try{
            Iterator<Money> moneyIterator = this.bankServiceBlockingStub.withDraw(withDrawRequest);
            moneyIterator.forEachRemaining(item->{
                log.info("Received Money - {}",item.getValue());
            });
            log.info("Ended of the Blocking Stub Client!!");
        }catch (Exception ex){
            Status status = Status.fromThrowable(ex);
            log.info(status.getCode() + " : " + status.getDescription());
        }
    }

    @Test
    public void WithdrawTestAsync() throws InterruptedException {
        WithDrawRequest withDrawRequest = WithDrawRequest.newBuilder()
                .setAccountNumber(60)
                .setAmount(10)
                .setDenomination(25)
                .build();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        StreamObserverMoneyImpl responseObserver = new StreamObserverMoneyImpl(countDownLatch);
              this.bankServiceStubAsync.withDraw(withDrawRequest,responseObserver);
        log.info("Ended of the Non Blocking Stub Client!!");
      //This is not the right way to Sleep in Unit test
       // Uninterruptibles.sleepUninterruptibly(1200 , TimeUnit.MILLISECONDS);
        countDownLatch.await();

    }

    @Test
    public void CashDepositTest() throws InterruptedException {
        //We need to use Non Blocking Stub Only for client Streaming
        CountDownLatch countDownLatch = new CountDownLatch(1);
        StreamObserver<Balance> balanceObserver = new StreamObserverBalanceImpl(countDownLatch);
        StreamObserver<DepositRequest> depositRequestStreamObserver = this.bankServiceStubAsync.cashDeposit(balanceObserver);
        for (int i = 0; i < 10; i++) {
            DepositRequest depositRequest = DepositRequest.newBuilder()
                    .setAccountNumber(100)
                    .setDenomination(10)
                    .setTotalCount(5)
                    .build();
            //Sending Money to Server
            depositRequestStreamObserver.onNext(depositRequest);
        }
        //I am done sending Money to Server
        depositRequestStreamObserver.onCompleted();
        log.info("Balance Transfer Money Deposit completed !!");
        countDownLatch.await();

    }
}
