package org.example;

import com.example.models.TransferResponse;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;

@Slf4j
public class StreamObserverTransferRespImpl implements StreamObserver<TransferResponse> {

    private CountDownLatch countDownLatch;

    public StreamObserverTransferRespImpl(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void onNext(TransferResponse transferResponse) {
        log.info("Response Received - {}", transferResponse);
    }

    @Override
    public void onError(Throwable throwable) {
        this.countDownLatch.countDown();
    }

    @Override
    public void onCompleted() {
        this.countDownLatch.countDown();
        log.info("Response stream is completed !!!");
    }
}
