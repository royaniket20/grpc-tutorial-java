package org.example.metadata;

import io.grpc.CallCredentials;
import io.grpc.Metadata;
import io.grpc.Status;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Executor;

import static org.example.metadata.ClientConstant.USER_TOKEN;

@Slf4j
public class UserSessionToken extends CallCredentials {

    private String jwt;

    public UserSessionToken(String jwt) {
        this.jwt = jwt + "_"+Thread.currentThread().getName();
    }

    @Override
    public void applyRequestMetadata(RequestInfo requestInfo, Executor executor, MetadataApplier metadataApplier) {

       //This call should be super quick - So they provide you with a managed executor - so that this cannot get blocked
        //And put pressure on your overall application
        executor.execute(()->{
            log.info("Adding JWT token for User ");
            Metadata metadata = new Metadata();
            metadata.put(USER_TOKEN ,this.jwt );
            metadataApplier.apply(metadata);
            //Also if something fails - you can Just do Like
            //metadataApplier.fail(Status.ABORTED); //SOMETHING LIKE THIS IN CATCH BLOCK
        });

    }

    @Override
    public void thisUsesUnstableApi() {

    }
}
