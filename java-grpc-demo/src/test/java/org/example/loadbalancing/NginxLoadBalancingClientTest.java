package org.example.loadbalancing;

import com.example.models.Balance;
import com.example.models.BalanceCheckRequest;
import com.example.models.BankServiceGrpc;
import com.example.models.DepositRequest;
import com.example.models.ErrorResponse;
import com.example.models.Money;
import com.example.models.WithDrawRequest;
import com.google.common.util.concurrent.Uninterruptibles;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.protobuf.ProtoUtils;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.example.StreamObserverBalanceImpl;
import org.example.StreamObserverMoneyImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
public class NginxLoadBalancingClientTest {

    private BankServiceGrpc.BankServiceBlockingStub bankServiceBlockingStub;
//Setting Up client
    @BeforeAll
    public void setup(){
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("localhost", 8585)
                .usePlaintext()
                .build();
       this.bankServiceBlockingStub = BankServiceGrpc.newBlockingStub(managedChannel);
    }

    @Test
    public void BalanceTest(){
        try{

            for (int i = 0; i < 50; i++) {
                Uninterruptibles.sleepUninterruptibly(500 , TimeUnit.MILLISECONDS);
                BalanceCheckRequest balanceCheckRequest = BalanceCheckRequest.newBuilder()
                        .setAccountNumber(i+100)
                        .build();
                Balance balance = this.bankServiceBlockingStub.getBalance(balanceCheckRequest);
                log.info("Received Balance - {}", balance.getAmount());
            }
        }catch (Exception ex){
            Status status = Status.fromThrowable(ex);
            log.info(status.getCode() + " : " + status.getDescription());
            Metadata metadata = Status.trailersFromThrowable(ex);
            ErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ErrorResponse.getDefaultInstance()));
            log.info(errorResponse.getStatus() + " : " + errorResponse.getMessage());
        }

        //TODO - mORE ERROR HANDLING example here -https://www.vinsguru.com/grpc-error-handling/

    }


}
