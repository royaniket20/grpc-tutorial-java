package org.example.loadbalancing;

import com.example.models.Balance;
import com.example.models.BalanceCheckRequest;
import com.example.models.BankServiceGrpc;
import com.example.models.ErrorResponse;
import com.google.common.util.concurrent.Uninterruptibles;
import io.grpc.*;
import io.grpc.protobuf.ProtoUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.TimeUnit;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
public class ClientSideLoadBalancing {


    private BankServiceGrpc.BankServiceBlockingStub bankServiceBlockingStub;
    //Setting Up client
    //TODO Read more on - https://sultanov.dev/blog/grpc-client-side-load-balancing/
    @BeforeAll
    public void setup(){
        NameResolver.Factory nameResolverFactory = new MultiAddressNameResolverFactory(
                new InetSocketAddress("localhost", 6565),
                new InetSocketAddress("localhost", 7575)
        );
        ManagedChannel managedChannel = ManagedChannelBuilder
                .forTarget("bank-service")
                /**
                 * In the case of the load balancing policy, we do not need to implement anything ourselves, we can choose one of the policies presented in the library, e.g. grpclb, pick_first or round_robin. Although the first two policies do not suit us, a Round Robin will work just fine.
                 */
                .defaultLoadBalancingPolicy("round_robin")
                .nameResolverFactory(nameResolverFactory)
                //.forAddress("localhost", 8585)
                .usePlaintext()
                .build();
        this.bankServiceBlockingStub = BankServiceGrpc.newBlockingStub(managedChannel);
    }

    @Test
    public void BalanceTest(){
        try{

            for (int i = 0; i < 50; i++) {
                Uninterruptibles.sleepUninterruptibly(500 , TimeUnit.MILLISECONDS);
                BalanceCheckRequest balanceCheckRequest = BalanceCheckRequest.newBuilder()
                        .setAccountNumber(i+100)
                        .build();
                Balance balance = this.bankServiceBlockingStub.getBalance(balanceCheckRequest);
                log.info("Received Balance - {}", balance.getAmount());
            }
        }catch (Exception ex){
            Status status = Status.fromThrowable(ex);
            log.info(status.getCode() + " : " + status.getDescription());
            Metadata metadata = Status.trailersFromThrowable(ex);
            ErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ErrorResponse.getDefaultInstance()));
            log.info(errorResponse.getStatus() + " : " + errorResponse.getMessage());
        }

        //TODO - mORE ERROR HANDLING example here -https://www.vinsguru.com/grpc-error-handling/

    }
}
