package org.example.deadline;

import io.grpc.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Slf4j
public class DeadlineInterceptor implements ClientInterceptor {
    @Override
    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> methodDescriptor, CallOptions callOptions, Channel channel) {
        log.info("Interceptor is Being called ");
        callOptions = callOptions.withDeadline(Deadline.after(1000, TimeUnit.MILLISECONDS));
        //We can apply for specific Case also
        if(Objects.isNull(callOptions.getDeadline())){
            //Someone has already Set a Deadline
        }
        return channel.newCall(methodDescriptor,callOptions); // Do Nothing here
    }
}
