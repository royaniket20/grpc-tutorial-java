package org.example.deadline;

import com.example.models.*;
import io.grpc.*;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.example.StreamObserverBalanceImpl;
import org.example.StreamObserverMoneyImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
public class BankClientDeadLineTest {

    private BankServiceGrpc.BankServiceBlockingStub bankServiceBlockingStub;
    private BankServiceGrpc.BankServiceStub bankServiceStubAsync;
//Setting Up client
    @BeforeAll
    public void setup(){
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("localhost", 5656)
                .usePlaintext()
                .intercept(new DeadlineInterceptor()) //Now we can remove Deadline on all test methods
                .build();
        log.info(" --- CHANNEL IS CREATED ----------"); //But they get just created - It will Not fail even if no server running on 5656
       this.bankServiceBlockingStub = BankServiceGrpc.newBlockingStub(managedChannel);
       this.bankServiceStubAsync = BankServiceGrpc.newStub(managedChannel);
        log.info(" --- BANKING SYNC AND ASYNC STUB IS CREATED ----------");
    }

    @Test
    public void BalanceTest(){
        try{
        BalanceCheckRequest balanceCheckRequest = BalanceCheckRequest.newBuilder()
                .setAccountNumber(406)
                .build();
        Balance balance = this.bankServiceBlockingStub
                //.withDeadline(Deadline.after(4500,TimeUnit.MILLISECONDS)) //Moved to Interceptor
                .getBalance(balanceCheckRequest);
        log.info("Received Balance - {}", balance.getAmount());

        }catch (Exception ex){
            Status status = Status.fromThrowable(ex);
            log.info(status.getCode() + " : " + status.getDescription());
            //Handle some fallback value here
        }

        //TODO - mORE ERROR HANDLING example here -https://www.vinsguru.com/grpc-error-handling/

    }

    @Test
    public void WithdrawTestBlocking(){
        WithDrawRequest withDrawRequest = WithDrawRequest.newBuilder()
                .setAccountNumber(58)
                .setAmount(10)
                .setDenomination(25)
                .build();
        try{
            Iterator<Money> moneyIterator = this.bankServiceBlockingStub
                    //.withDeadline(Deadline.after(1000,TimeUnit.MILLISECONDS)) ////Moved to Interceptor - But you can override Global Settings here
                    //Now in the above the Client may stop listening to stream due to timeout
                    //But Server still may be emitting to space -
                    //We need to stop server from doing so using Context
                    .withDraw(withDrawRequest);
            moneyIterator.forEachRemaining(item->{
                log.info("Received Money - {}",item.getValue());
            });
            log.info("Ended of the Blocking Stub Client!!");
        }catch (Exception ex){
            Status status = Status.fromThrowable(ex);
            log.info(status.getCode() + " : " + status.getDescription());
        }
    }

    @Test
    public void WithdrawTestAsync() throws InterruptedException {
        WithDrawRequest withDrawRequest = WithDrawRequest.newBuilder()
                .setAccountNumber(60)
                .setAmount(10)
                .setDenomination(25)
                .build();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        StreamObserverMoneyImpl responseObserver = new StreamObserverMoneyImpl(countDownLatch);
              this.bankServiceStubAsync.withDraw(withDrawRequest,responseObserver);
        log.info("Ended of the Non Blocking Stub Client!!");
      //This is not the right way to Sleep in Unit test
       // Uninterruptibles.sleepUninterruptibly(1200 , TimeUnit.MILLISECONDS);
        countDownLatch.await();

    }

    @Test
    public void CashDepositTest() throws InterruptedException {
        //We need to use Non Blocking Stub Only for client Streaming
        CountDownLatch countDownLatch = new CountDownLatch(1);
        StreamObserver<Balance> balanceObserver = new StreamObserverBalanceImpl(countDownLatch);
        StreamObserver<DepositRequest> depositRequestStreamObserver = this.bankServiceStubAsync.cashDeposit(balanceObserver);
        for (int i = 0; i < 10; i++) {
            DepositRequest depositRequest = DepositRequest.newBuilder()
                    .setAccountNumber(100)
                    .setDenomination(10)
                    .setTotalCount(5)
                    .build();
            //Sending Money to Server
            depositRequestStreamObserver.onNext(depositRequest);
        }
        //I am done sending Money to Server
        depositRequestStreamObserver.onCompleted();
        log.info("Balance Transfer Money Deposit completed !!");
        countDownLatch.await();

    }
}
