package org.example;

import com.example.models.Balance;
import com.example.models.BalanceCheckRequest;
import com.example.models.BankServiceGrpc;
import com.example.models.DepositRequest;
import com.example.models.ErrorResponse;
import com.example.models.Money;
import com.example.models.TransferRequest;
import com.example.models.TransferResponse;
import com.example.models.TransferServiceGrpc;
import com.example.models.WithDrawRequest;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.protobuf.ProtoUtils;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
public class TransferClientTest {

    private TransferServiceGrpc.TransferServiceStub transferServiceStub;

    //Setting Up client
    @BeforeAll
    public void setup() {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("localhost", 5656)
                .usePlaintext()
                .build();
        this.transferServiceStub = TransferServiceGrpc.newStub(managedChannel);
    }

    @Test
    public void TransferServiceTest() throws InterruptedException {

            CountDownLatch countDownLatch = new CountDownLatch(1);
            StreamObserver<TransferResponse> responseObserver = new StreamObserverTransferRespImpl(countDownLatch);
            StreamObserver<TransferRequest> transferRequestStreamObserver = this.transferServiceStub.transferMoney(responseObserver);
            try {
                for (int i = 0; i < 10; i++) {
                    TransferRequest transferRequest = TransferRequest.newBuilder()
                            .setAmount(10)
                            .setFromAccount(100)
                            .setToAccount(200)
                            .build();
                    transferRequestStreamObserver.onNext(transferRequest);
                }
                transferRequestStreamObserver.onCompleted();
            }catch (Exception ex){
                transferRequestStreamObserver.onError(ex);
            }
        countDownLatch.await();

    }


}
