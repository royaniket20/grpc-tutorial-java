package org.example.server.ssl;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder;
import io.grpc.netty.shaded.io.netty.handler.ssl.SslContext;
import io.grpc.netty.shaded.io.netty.handler.ssl.SslContextBuilder;
import io.grpc.protobuf.services.ProtoReflectionService;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.net.URL;

@Slf4j
public class GrpcServer {
//Create Grpc Server
    public static void main(String[] args) throws IOException, InterruptedException {
        int portNum = 5656;
      log.info("*********** S T A R T I N G   G R P C   S E R V E R ****************");
        SslContext sslContext = GrpcSslContexts.configure(SslContextBuilder.forServer(getFileByPath("Certs/localhost.crt"),getFileByPath("Certs/localhost.pem"))).build();
       // Server server = ServerBuilder.forPort(portNum) //ServerBuilder is an Abstract class - we now use a concreate impl
        Server server = NettyServerBuilder.forPort(portNum)
                .sslContext(sslContext)
                .addService(ProtoReflectionService.newInstance())
                .addService(new BankSSLService())
                .build();
        log.info("1. SERVER IS CONFIGURED TO RUN ON PORT " + portNum);
        server.start();
        log.info("2. SERVER IS STARTED TO RUN ON PORT " + portNum);
        server.awaitTermination();
        log.info("3. SERVER IS STOPPED ON PORT " + portNum);
    }


    private static File getFileByPath(String filePath){
        ClassLoader classLoader = GrpcServer.class.getClassLoader();
        URL resource = classLoader.getResource(filePath);
        return new File(resource.getFile());
    }
}
