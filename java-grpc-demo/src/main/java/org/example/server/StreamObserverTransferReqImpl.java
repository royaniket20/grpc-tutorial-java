package org.example.server;

import com.example.models.Account;
import com.example.models.TransferRequest;
import com.example.models.TransferResponse;
import com.example.models.TransferStatus;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StreamObserverTransferReqImpl  implements StreamObserver<TransferRequest> {

    private StreamObserver<TransferResponse> transferResponseStreamObserver;
    private Integer counter;

    public StreamObserverTransferReqImpl(StreamObserver<TransferResponse> transferResponseStreamObserver) {
        this.transferResponseStreamObserver = transferResponseStreamObserver;
        this.counter = 0;
    }

    @Override
    public void onNext(TransferRequest transferRequest) {
        Integer fromAccount = transferRequest.getFromAccount();
        Integer toAccount = transferRequest.getToAccount();
        Integer balance = AccountDatabase.getBalance(fromAccount);
        Integer amountToBeTransferred = transferRequest.getAmount();
        TransferStatus transferStatus = TransferStatus.FAILED;
        if(balance>amountToBeTransferred && fromAccount.intValue() !=toAccount.intValue()){
            AccountDatabase.deductBalance(fromAccount,amountToBeTransferred);
            AccountDatabase.addBalance(toAccount,amountToBeTransferred);
            transferStatus = TransferStatus.SUCCESS;
        }
        Account fromAccountObj = Account.newBuilder()
                .setAccount(fromAccount)
                .setBalance(AccountDatabase.getBalance(fromAccount))
                .build();
        Account toAccountObj = Account.newBuilder()
                .setAccount(toAccount)
                .setBalance(AccountDatabase.getBalance(toAccount))
                .build();
        TransferResponse transferResponse = TransferResponse.newBuilder()
                .setStatus(transferStatus)
                .addAccounts(fromAccountObj)
                .addAccounts(toAccountObj)
                .build();
        this.counter++;
        if(this.counter%2 ==0)
        {
            log.info("Message transmitted to Client --- ");
            this.transferResponseStreamObserver.onNext(transferResponse);
        }
    }

    @Override
    public void onError(Throwable throwable) {
        this.transferResponseStreamObserver.onError(new RuntimeException("Something Bad happened !!"));
    }

    @Override
    public void onCompleted() {
        log.info("Message Transmission to Client completed !!");
        this.transferResponseStreamObserver.onCompleted();
    }
}
