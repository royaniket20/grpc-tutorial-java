package org.example.server;


import com.example.models.Balance;
import com.example.models.BalanceCheckRequest;
import com.example.models.BankServiceGrpc;
import com.example.models.DepositRequest;
import com.example.models.ErrorResponse;
import com.example.models.HttpStatus;
import com.example.models.Money;
import com.example.models.WithDrawRequest;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.protobuf.ProtoUtils;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class BankService extends BankServiceGrpc.BankServiceImplBase {


    //Return type is Void - Becuase We have to return Result via
    //StreamObserver Object

    /**
     * StreamObserver Have 3 Mothods
     * onNext - Data channel
     * onError - Error Channel
     * onCompleted - When Successfully Finished - Call this to close the response channel
     */

    @Override
    public void getBalance(BalanceCheckRequest request, StreamObserver<Balance> responseObserver) {
        log.info("Endpoint got called for checking Balance !!");
        log.info("Received BalanceCheckRequest = {}", request);
        try {
            if (request.getAccountNumber() > 499) {
                throw new RuntimeException("Invalid Account Number Given - " + request.getAccountNumber());
            }
            Balance balance = Balance.newBuilder()
                    .setAmount(AccountDatabase.getBalance(request.getAccountNumber()))
                    .build();
            responseObserver.onNext(balance);
            responseObserver.onCompleted();
        } catch (Exception ex) {
            log.error("Exception Occurred - {}", ex.getMessage());
            Metadata metadata = new Metadata();
            Metadata.Key<ErrorResponse> responseKey = ProtoUtils.keyForProto(ErrorResponse.getDefaultInstance());
            metadata.put(
                    responseKey,
                    ErrorResponse.newBuilder()
                            .setStatus(HttpStatus.BAD_REQUEST)
                            .setMessage(ex.getMessage())
                            .build()
            );
            Status status = Status.INVALID_ARGUMENT.withDescription(ex.getMessage());
            responseObserver.onError(status.asRuntimeException(metadata));
        }
    }


    @Override
    public void withDraw(WithDrawRequest request, StreamObserver<Money> responseObserver) {

        try {
            log.info("Endpoint got called for withdraw  Balance !!");
            log.info("Received WithDrawRequest = {}", request);
            Integer accountNumber = request.getAccountNumber();
            Integer amount = request.getAmount();
            Integer denomination = request.getDenomination();
            Integer balance = AccountDatabase.getBalance(accountNumber);
            if (balance > denomination) {
                for (int index = 0; index < amount; index++) {
                    log.info("Sending to client USD " + denomination);
                    Money money = Money.newBuilder()
                            .setValue(denomination)
                            .build();
                    responseObserver.onNext(money);
                    AccountDatabase.deductBalance(accountNumber, denomination);
                    slowProcess();
                    balance = balance - denomination;
                    if (balance < denomination) {
                        throw new RuntimeException(" Balance is Now below requirement - Aborting Rquest");
                    }
                }
                responseObserver.onCompleted();
            } else {
                throw new RuntimeException("Balance is below the requested amount");
            }
        } catch (Exception ex) {
            Status status = Status.CANCELLED.withDescription(ex.getMessage());
            responseObserver.onError(status.asRuntimeException());
        }
    }


    //Here we can see we have a Return Type - Which is Noting But Input Stream
    //But Response is Still coming via responseObserver - which is parameter
    //Server need to provide Impl for StreamObserver<DepositRequest> to receive Client stream
    @Override
    public StreamObserver<DepositRequest> cashDeposit(StreamObserver<Balance> responseObserver) {

        StreamObserverDepositRequestImpl streamObserverDepositRequest = new StreamObserverDepositRequestImpl(responseObserver);
        log.info("Endpoint got called for cashDeposit !!");
        return streamObserverDepositRequest;
    }

    private static void slowProcess() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
