package org.example.server;

import com.example.models.TransferRequest;
import com.example.models.TransferResponse;
import com.example.models.TransferServiceGrpc;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TransferService extends TransferServiceGrpc.TransferServiceImplBase {

    @Override
    public StreamObserver<TransferRequest> transferMoney(StreamObserver<TransferResponse> responseObserver) {
          return new StreamObserverTransferReqImpl(responseObserver);
    }
}
