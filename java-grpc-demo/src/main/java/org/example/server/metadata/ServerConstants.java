package org.example.server.metadata;

import io.grpc.Context;
import io.grpc.Metadata;

public class ServerConstants {
    public static final Metadata.Key<String> USER_TOKEN = Metadata.Key.of("user-token" , Metadata.ASCII_STRING_MARSHALLER);

    public static final Metadata.Key<String> CLIENT_KEY =  Metadata.Key.of("client-token" , Metadata.ASCII_STRING_MARSHALLER);
    public static final Context.Key<String> CONTEXT_USER_TOKEN = Context.key("CONTEXT_USER_TOKEN");
    public static final Context.Key<String> CONTEXT_CLIENT_TOKEN = Context.key("CONTEXT_CLIENT_TOKEN");
    //Also - CONTEXT_USER_TOKEN is thread safe for each user

    //Although both are Lookng same - actually we cannot get value using CONTEXT_USER_TOKEN_DUMMY
    public static final Context.Key<String> CONTEXT_USER_TOKEN_DUMMY = Context.key("CONTEXT_USER_TOKEN");
}
