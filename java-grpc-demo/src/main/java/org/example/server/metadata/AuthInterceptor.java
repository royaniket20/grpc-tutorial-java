package org.example.server.metadata;

import io.grpc.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Random;

import static org.example.server.metadata.ServerConstants.CONTEXT_CLIENT_TOKEN;
import static org.example.server.metadata.ServerConstants.CLIENT_KEY;

@Slf4j
public class AuthInterceptor implements ServerInterceptor {
    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> serverCall, Metadata metadata, ServerCallHandler<ReqT, RespT> serverCallHandler) {
        String bank_token = metadata.get(CLIENT_KEY);
        //log.info("Client token Received - {}",bank_token);
        if(tokenValidation(bank_token)){
            Context contextClientToken = Context.current().withValue(
                    CONTEXT_CLIENT_TOKEN, bank_token
            );
            //Now to Pass the context to Business layer you need to Call via context
            //return serverCallHandler.startCall(serverCall,metadata);
            return  Contexts.interceptCall(contextClientToken,serverCall,metadata,serverCallHandler);
        }else{
            Status invalidToken = Status.UNAUTHENTICATED.withDescription("Invalid Client Token");
             serverCall.close(invalidToken,metadata);
        }
        //you should not return null
        return  new ServerCall.Listener<ReqT>() {
            @Override
            public void onMessage(ReqT message) {
                super.onMessage(message);
            }
        };
    }

    private  boolean tokenValidation(String token){
        if(token == null){
            return  false;
        }else{
            return token.equals("bank-client-secret");
        }
    }
}
