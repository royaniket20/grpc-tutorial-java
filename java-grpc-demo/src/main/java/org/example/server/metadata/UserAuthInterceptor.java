package org.example.server.metadata;

import io.grpc.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Random;

import static org.example.server.metadata.ServerConstants.USER_TOKEN;

@Slf4j
public class UserAuthInterceptor implements ServerInterceptor {

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> serverCall, Metadata metadata, ServerCallHandler<ReqT, RespT> serverCallHandler) {
        String user_token = metadata.get(USER_TOKEN);
        //log.info("User token Received - {}",user_token);
        if(tokenValidation(user_token)){
            Context contextClientToken = Context.current().withValue(
                    ServerConstants.CONTEXT_USER_TOKEN, user_token
            );
            //Now to Pass the context to Business layer you need to Call via context
            //return serverCallHandler.startCall(serverCall,metadata);
            return  Contexts.interceptCall(contextClientToken,serverCall,metadata,serverCallHandler);
        }else{
            Status invalidToken = Status.UNAUTHENTICATED.withDescription("Invalid User Token");
             serverCall.close(invalidToken,metadata);
        }
        //you should not return null
        return  new ServerCall.Listener<ReqT>() {
            @Override
            public void onMessage(ReqT message) {
                super.onMessage(message);
            }
        };
    }

    private  boolean tokenValidation(String token){
      return true; //Dummy Validation
    }
}
