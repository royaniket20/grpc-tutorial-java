package org.example.server.metadata;


import com.example.models.*;
import com.google.common.util.concurrent.Uninterruptibles;
import io.grpc.Context;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.protobuf.ProtoUtils;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.example.server.AccountDatabase;
import org.example.server.StreamObserverDepositRequestImpl;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.example.server.metadata.ServerConstants.CONTEXT_CLIENT_TOKEN;
import static org.example.server.metadata.ServerConstants.CONTEXT_USER_TOKEN;


@Slf4j
public class BankMetadataService extends BankServiceGrpc.BankServiceImplBase {


    //Return type is Void - Becuase We have to return Result via
    //StreamObserver Object

    /**
     * StreamObserver Have 3 Mothods
     * onNext - Data channel
     * onError - Error Channel
     * onCompleted - When Successfully Finished - Call this to close the response channel
     */

    @Override
    public void getBalance(BalanceCheckRequest request, StreamObserver<Balance> responseObserver) {
        log.info(Thread.currentThread().getName() + " : Endpoint got called for checking Balance !!");
        log.info(Thread.currentThread().getName() + " : Received BalanceCheckRequest = {}", request);

        //Simulate Load for Deadline Demo
        Uninterruptibles.sleepUninterruptibly(new Random().nextInt(500)+1000, TimeUnit.MILLISECONDS);
        try {
            if (request.getAccountNumber() > 499) {
                throw new RuntimeException("Invalid Account Number Given - " + request.getAccountNumber());
            }
            String client_token = CONTEXT_CLIENT_TOKEN.get();
            String user_token = CONTEXT_USER_TOKEN.get();
            log.info(Thread.currentThread().getName()+": Client Token - {} | User Token - {} ",client_token,user_token);
            Balance balance = Balance.newBuilder()
                    .setAmount(AccountDatabase.getBalance(request.getAccountNumber()))
                    .build();
            responseObserver.onNext(balance);
            responseObserver.onCompleted();
        } catch (Exception ex) {
            log.error("Exception Occurred - {}", ex.getMessage());
            Metadata metadata = new Metadata();
            Metadata.Key<ErrorResponse> responseKey = ProtoUtils.keyForProto(ErrorResponse.getDefaultInstance());
            metadata.put(
                    responseKey,
                    ErrorResponse.newBuilder()
                            .setStatus(HttpStatus.BAD_REQUEST)
                            .setMessage(ex.getMessage())
                            .build()
            );
            Status status = Status.INVALID_ARGUMENT.withDescription(ex.getMessage());
            responseObserver.onError(status.asRuntimeException(metadata));
        }
    }


    @Override
    public void withDraw(WithDrawRequest request, StreamObserver<Money> responseObserver) {

        try {
            log.info("Endpoint got called for withdraw  Balance !!");
            log.info("Received WithDrawRequest = {}", request);
            Integer accountNumber = request.getAccountNumber();
            Integer amount = request.getAmount();
            Integer denomination = request.getDenomination();
            Integer balance = AccountDatabase.getBalance(accountNumber);
            if (balance > denomination) {
                for (int index = 0; index < amount; index++) {

                    //Stop Grpc to emit data when Client is Not listen anymore

                    if(!Context.current().isCancelled()) {

                        log.info("Sending to client USD " + denomination);
                        Money money = Money.newBuilder()
                                .setValue(denomination)
                                .build();
                        responseObserver.onNext(money);
                        AccountDatabase.deductBalance(accountNumber, denomination);
                        slowProcess();
                        balance = balance - denomination;
                        if (balance < denomination) {
                            throw new RuntimeException(" Balance is Now below requirement - Aborting Rquest");
                        }
                    }else{
                        log.info("CONTEXT IS CLOSED - I AM STOPPING NOW !!! ");
                        break;
                    }
                }
                responseObserver.onCompleted();
            } else {
                throw new RuntimeException("Balance is below the requested amount");
            }
        } catch (Exception ex) {
            Status status = Status.CANCELLED.withDescription(ex.getMessage());
            responseObserver.onError(status.asRuntimeException());
        }
    }


    //Here we can see we have a Return Type - Which is Noting But Input Stream
    //But Response is Still coming via responseObserver - which is parameter
    //Server need to provide Impl for StreamObserver<DepositRequest> to receive Client stream
    @Override
    public StreamObserver<DepositRequest> cashDeposit(StreamObserver<Balance> responseObserver) {

        StreamObserverDepositRequestImpl streamObserverDepositRequest = new StreamObserverDepositRequestImpl(responseObserver);
        log.info("Endpoint got called for cashDeposit !!");
        return streamObserverDepositRequest;
    }

    private static void slowProcess() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
