package org.example.server;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
/**
 * This is a static Database Kind of type
 */
public class AccountDatabase {

    private  AccountDatabase(){} // Prevent Object creation
    private  static final Map<Integer , Integer> account_balance_db;

    static {
        account_balance_db = IntStream.rangeClosed(1,500)
                .boxed()
                .collect(Collectors.toMap(Function.identity() ,item->item*5 ));
    log.info("DB IS INITIALIZED **************** ");
    account_balance_db.entrySet().stream().forEach(item->{
        log.info("Account : {} ==> Balance : {}",item.getKey(),item.getValue());
    });
    }

    public static Integer getBalance(Integer accountNumber){
        log.info("Getting account Balance for account Number - {}",accountNumber);
        if(account_balance_db.get(accountNumber)!=null){
            return  account_balance_db.get(accountNumber);
        }else{
            throw new RuntimeException("Account Number "+accountNumber+" is not present !!");
        }
    }

    public static Integer addBalance(Integer accountNumber, Integer balance){
        log.info("Adding  account Balance {} into  account Number - {}",balance,accountNumber);
      return   account_balance_db.computeIfPresent(accountNumber , (currentAccountNumber,currbalance)-> currbalance+ balance );
    }

    public static Integer deductBalance(Integer accountNumber, Integer balance){
        log.info("Deducting  account Balance {} from  account Number - {}",balance,accountNumber);
        return   account_balance_db.computeIfPresent(accountNumber , (currentAccountNumber,currbalance)-> {
            if(currbalance>balance){
                return   currbalance- balance;
            }else{
                throw new RuntimeException("Not having enough balance to Deduct");
            }

        } );
    }
}
