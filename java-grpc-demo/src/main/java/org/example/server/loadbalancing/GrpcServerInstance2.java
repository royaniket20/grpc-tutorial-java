package org.example.server.loadbalancing;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.protobuf.services.ProtoReflectionService;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class GrpcServerInstance2 {
//Create Grpc Server
    public static void main(String[] args) throws IOException, InterruptedException {
        int portNum = 7575;
      log.info("*********** S T A R T I N G   G R P C   S E R V E R ****************");
        Server server = ServerBuilder.forPort(portNum)
                .addService(ProtoReflectionService.newInstance())
                .addService(new BankServiceLoadBalanced())
                .build();
        log.info("1. SERVER IS CONFIGURED TO RUN ON PORT " + portNum);
        server.start();
        log.info("2. SERVER IS STARTED TO RUN ON PORT " + portNum);
        server.awaitTermination();
        log.info("3. SERVER IS STOPPED ON PORT " + portNum);
    }
}
