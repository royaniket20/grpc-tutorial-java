package org.example.server;

import com.example.models.Balance;
import com.example.models.DepositRequest;
import com.google.common.util.concurrent.Uninterruptibles;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class StreamObserverDepositRequestImpl implements StreamObserver<DepositRequest> {

    private Integer accountBalance;
    private StreamObserver<Balance> balanceStreamObserver;

    public StreamObserverDepositRequestImpl(StreamObserver<Balance> balanceStreamObserver) {
        this.balanceStreamObserver = balanceStreamObserver;
    }

    @Override
    public void onNext(DepositRequest depositRequest) {
        Integer accountNumber = depositRequest.getAccountNumber();
        Integer countOfNotes = depositRequest.getTotalCount();
        Integer denomination = depositRequest.getDenomination();
        while (countOfNotes>0)
        {
            Integer updatedBalance = AccountDatabase.addBalance(accountNumber, denomination);
            this.accountBalance = updatedBalance;
            Uninterruptibles.sleepUninterruptibly(100, TimeUnit.MILLISECONDS);
            countOfNotes--;
        }

    }

    @Override
    public void onError(Throwable throwable) {
  //Need to implement
    }

    @Override
    public void onCompleted() {
        log.info("Client have done sending all the Money to Bank !!");
        Balance balance = Balance.newBuilder()
                .setAmount(this.accountBalance)
                .build();
        this.balanceStreamObserver.onNext(balance);
        this.balanceStreamObserver.onCompleted();
    }
}
