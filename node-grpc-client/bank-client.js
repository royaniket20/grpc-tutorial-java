const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader')

const packageDef = protoLoader.loadSync('proto/bank-service.proto',{
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
   
})

const protoDesc = grpc.loadPackageDefinition(packageDef)

const client  = new protoDesc.BankService('localhost:5656', grpc.credentials.createInsecure())

client.getBalance({account_number : 400} , (err, balance)=> {

if(err){
    console.error("Error Happened - "+ err.message)
}else {
    console.log("Balance available - "+ balance.amount);
}    

})

client.getBalance({account_number : 555} , (err, balance)=> {

    if(err){
        console.error("Error Happened - "+ err.message)
    }else {
        console.log("Balance available - "+ balance.amount);
    }    
    
    })
