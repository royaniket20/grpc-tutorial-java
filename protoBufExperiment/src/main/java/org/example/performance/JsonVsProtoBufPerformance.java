package org.example.performance;

import com.example.models.Person;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.function.Consumer;

public class JsonVsProtoBufPerformance {


    public static void main(String[] args) throws IOException {
           //Remember Runnable are Just Normal Functional Interface
        Runnable jsonRunnable = null;
        Runnable protoBufRunnable;
        Thread th = new Thread();
        th.start();
        //To do the performance test
        //Json serialization and Deserialization

        jsonRunnable = () -> {
            JsonPerson person = new JsonPerson();
            person.setName("Aniket");
            person.setAge(30);
            ObjectMapper objectMapper = new ObjectMapper();
            byte[] jPersonBytes = new byte[0];
            try {
                jPersonBytes = objectMapper.writeValueAsBytes(person);
                JsonPerson personDeserilized = objectMapper.readValue(jPersonBytes, JsonPerson.class);
            } catch (Exception e) {
              e.printStackTrace();
            }
        };

        protoBufRunnable = ()->{
            try{
                Person person = Person.newBuilder()
                        .setAge(30)
                        .setName("Aniket")
                        .build();
                byte[] personBytes = new byte[0];
                personBytes = person.toByteArray();
                Person personDeserilized = Person.parseFrom(personBytes);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        };
        for (int i = 0; i < 5; i++) {
            runPerformanceTest(jsonRunnable , "jsonRunnable");
            runPerformanceTest(protoBufRunnable , "protoBufRunnable");
        }

    }


    private static void runPerformanceTest(Runnable runnable , String methodName){
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1_000_000; i++) {
            runnable.run();
        }
        long end = System.currentTimeMillis();
        System.out.println("Time taken by "+ methodName + " is : "+(end-start));
    }


}
