package org.example;
import com.example.models.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PersonDemo {

    public static void main(String[] args) throws IOException {
        System.out.println("Proto Buf Basic Demo");
        Person person  = Person.newBuilder()
                .setAge(32)
                .setName("Aniket Roy").build();
        System.out.println(person.toString());

        //Serilization and Desrilization
        System.out.println("Serialization of Person Obj ");
        Path path = Paths.get("person.ser");
        Files.write(path , person.toByteArray());
        System.out.println("DSerilization of Person Obj from File");
        path = Paths.get("person.ser");
        byte[] bytes = Files.readAllBytes(path);
        Person person1 = Person.parseFrom(bytes);
        System.out.println("DSerilized Person - " + person1);


    }
}
