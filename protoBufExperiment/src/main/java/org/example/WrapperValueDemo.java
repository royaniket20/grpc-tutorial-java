package org.example;

import com.example.models.Credentials;
import com.google.protobuf.Int32Value;

public class WrapperValueDemo {

    public static void main(String[] args) {
        Credentials credentials = Credentials.newBuilder()
                .setStatusCode(Int32Value.of(100))
                .build();
        checkValue(credentials);
        credentials = Credentials.newBuilder()
                .build();
        checkValue(credentials);
    }

    private static void checkValue(Credentials credentials){
        if(credentials.hasStatusCode()){
            System.out.println("Status Code is Set to "+ credentials.getStatusCode());
        }else{
            System.out.println("Status code is not set !!");
        }
    }
}
