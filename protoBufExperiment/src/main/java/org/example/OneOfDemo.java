package org.example;

import com.example.models.Credentials;
import com.example.models.EmailCred;
import com.example.models.PhoneCred;

public class OneOfDemo {

    public static void main(String[] args) {
        EmailCred emailCred = EmailCred.newBuilder()
                .setEmail("emaxple@xxx.com")
                .setPassword("#$%^5455")
                .build();

        PhoneCred phoneCred = PhoneCred.newBuilder()
                .setPhoneNumber(123254789)
                .setOtp(5566)
                .build();

        Credentials credentials = Credentials.newBuilder()
                .setEmail(emailCred)
                .build();
        login(credentials);

         credentials = Credentials.newBuilder()
                .setEmail(emailCred)
                 .setPhone(phoneCred) //If you Set Both
                .build();
        loginAll(credentials);


        //checking for Switch Case
        credentials = Credentials.newBuilder().build();
        findWhichOneSet(credentials);
        credentials = Credentials.newBuilder()
                .setEmail(emailCred)
                .build();
        findWhichOneSet(credentials);
        credentials = Credentials.newBuilder()
                .setPhone(phoneCred)
                .build();
        findWhichOneSet(credentials);
    }

    private static void login(Credentials credentials){
        System.out.println("Login method os called ");
        System.out.println(credentials.getEmail());
    }

    private static void loginAll(Credentials credentials){
        System.out.println("loginAll method os called ");
        System.out.println(credentials);
    }

    private static void findWhichOneSet(Credentials credentials){
        System.out.println("findWhichOneSet method os called ");
        switch (credentials.getCommunicationModeCase()) //This will create A enum for you
        {
            case EMAIL:
                System.out.println("EMAIL : "+credentials.getEmail());
                break;
            case PHONE:
                System.out.println("PHONE : "+credentials.getPhone());
                break;
            case COMMUNICATIONMODE_NOT_SET:
                System.out.println("COMMUNICATIONMODE_NOT_SET : "+credentials);
                break;
            default:
                System.out.println("default : "+"Something Bad happened");
        }
    }

}
