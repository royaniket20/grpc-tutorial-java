package org.example;

/*import com.example.models.Person;
import com.example.models.TVType;*/
import com.example.models.Television;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class VersionCompatibilityTest {

    public static void main(String[] args) throws IOException {
       /*
       Television television = Television.newBuilder()
                .setBrand("Sony")
                .setYear(2023)
                .build();
        System.out.println("Serialization of Television Obj ");
        Path path = Paths.get("television-v1.ser");
        Files.write(path , television.toByteArray());
        System.out.println("Deserialization of Television Obj from File");
        path = Paths.get("television-v1.ser");
        byte[] bytes = Files.readAllBytes(path);
        Television television1 = Television.parseFrom(bytes);
        System.out.println("Deserialize Television - " + television1);
        */
        System.out.println("Deserialization of Television Obj from File");
        Path pathv1 = Paths.get("television-v1.ser");
        byte[] bytes = Files.readAllBytes(pathv1);
        Television television1 = Television.parseFrom(bytes);
        System.out.println("Deserialize Television - " + television1.toString()); //This will Give misinformed Data


/*        Television television = Television.newBuilder()
                .setBrand("Sony")
                .setModel(2023)
                .setType(TVType.FULL_HD)
                .build();
        System.out.println("Serialization of Television Obj ");
        Path path = Paths.get("television-v2.ser");
        Files.write(path , television.toByteArray());*/
        System.out.println("Deserialization of Television Obj from File");
        Path path = Paths.get("television-v2.ser");
        byte[] bytes2 = Files.readAllBytes(path);
        Television television2 = Television.parseFrom(bytes2);
        System.out.println("Deserialize Television - " + television2.toString());


        /**
         *
         * Deserialization of Television Obj from File
         * Deserialize Television - brand: "Sony"
         * year: 2023
         * 3: 1 = This is Unknown Filed to Me as V1 Model of television
         * You do not have method to access this - as you are using Older model -
         * Need to Update the Model
         *
         */

    }
}
