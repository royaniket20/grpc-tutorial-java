package org.example.Composition;

import com.example.models.Address;
import com.example.models.Car;
import com.example.models.Human;

import java.util.ArrayList;
import java.util.List;

public class CompositionDemo {

    public static void main(String[] args) {
        System.out.println("Doing Composition ---- ");
        Car car = Car.newBuilder()
                .setMake("Honda")
                .setModel("V8-6")
                .setYear(2003)
                .build();
        List<Car> wishList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Car carTemp = Car.newBuilder()
                    .setMake("Honda-"+i)
                    .setModel("V8-"+i)
                    .setYear(2003+i)
                    .build();
            wishList.add(carTemp);
        }
        Address address = Address.newBuilder()
                .setCity("Kolkata")
                .setPostbox(711303)
                .setStreet("NH-6")
                .build();
        Human human = Human.newBuilder()
                .setAddress(address)
                .setAge(30)
                .setName("Aniket Roy")
                .setCar(car)
                .addAllWishList(wishList)
                .addWishList(car)
                .build();

        System.out.println("Created Human Composition - \n" + human.toString());

    }
}
