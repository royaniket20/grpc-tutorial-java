package org.example.Composition;

import com.example.models.Human;
import com.example.models.Person;

public class DefaultValueDemo {

    public static void main(String[] args) {
        Human person = Human.newBuilder().build();
        //Default value will get assigned
        System.out.println(person.getAddress().getCity() + " : City Name");
        System.out.println(person.getAge() + " : Age");
        //Has method is only for user defined types
        System.out.println(person.hasAddress() ? person.getAddress() : "No address Found ");
        //Default for primitive types - Is not possible
    }
}
