package org.example.Composition;

import com.example.models.BodyStyle;
import com.example.models.Car;
import com.example.models.Dealer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MapDemo {

    public static void main(String[] args) {
        List<Car> wishList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Car carTemp = Car.newBuilder()
                    .setMake("Honda-"+i)
                    .setModel("V8-"+i)
                    .setYear(2003+i)
                    .build();
            wishList.add(carTemp);
        }

        Dealer.Builder dealerBuilder = Dealer.newBuilder();
        wishList.stream().forEach(item->
        {
            dealerBuilder.putYearCarMapper(item.getYear(), item);
        });
        Dealer dealer = dealerBuilder.build();
        System.out.println(dealer);
        //This is the actual Map
        Map<Integer, Car> yearCarMapperMap = dealer.getYearCarMapperMap();
        System.out.println("Actual Map entries - "+ yearCarMapperMap);

        //We have not set BodyStyle - But let try to get it
        BodyStyle bodyStyle = yearCarMapperMap.get(2004).getBodyStyle();
        System.out.println(bodyStyle+ " Default Body Style"); // Even If you dONT SET PICK THE fIRST ONE

    }
}
